package practica5;

import java.util.regex.Pattern;
import java.util.regex.Matcher;


public class Regex02
{
    public static void main(String[] args)
    {
        String texto = "aa.ssdd.asddd.asd";//texto con numeros y letras
        String regex = "asd";//comprueba si contiene el patron "asd"

        Pattern patron = Pattern.compile(regex);
        Matcher concordancias = patron.matcher(texto);

        System.out.println("\nConcordancias: ");
        while(concordancias.find())
        {
            System.out.println("Indice: "
                    + concordancias.start()
                    + " Texto: " + concordancias.group()
                    + ")");
        }
    }
}