package practica5;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex07
{
    public static void main(String[] args)
    {
        String texto = "0123 aadd_   d1?f";//texto con numeros y letras
        String regex = "\\s";//comprueba si tiene espacios en blanco, saltos de linea o tabulaciones"

        Pattern patron = Pattern.compile(regex);
        Matcher concordancias = patron.matcher(texto);

        System.out.println("\nConcordancias: ");
        while(concordancias.find())
        {
            System.out.println("Indice: "
                    + concordancias.start()
                    + " Texto: " + concordancias.group()
                    + ")");
        }
    }
}