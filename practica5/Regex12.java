package practica5;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex12
{
    public static void main(String[] args)
    {
        String texto = "pepe@gamil.com";//texto con numeros y letras
        String regex = "([^@])+";//Comprueba si la cadena contiene uno o varios
                                // grupos de caracteres que no sea la @

        Pattern patron = Pattern.compile(regex);
        Matcher concordancias = patron.matcher(texto);

        System.out.println("\nConcordancias: ");
        while (concordancias.find()) {
            System.out.println("Indice: "
                    + concordancias.start()
                    + " Texto: " + concordancias.group()
                    + ")");
        }
    }
}