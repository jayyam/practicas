package practica5;

public class XamenPrueba
{
    /**
     * Clase principal de pruebas que contiene un vector de objetos de la clase Usuario
     * y métodos para las pruebas y manipulación del vector.
     */
        static int maxUsuarios = 7;
        static Usuario[] datosUsuarios = new Usuario[maxUsuarios];

        /**
         * Método principal que carga datos de prueba y coordina la
         * llamada a los distintos métodos probados.
         *
         * @param args
         */
        public static void main(String[] args) {
            //..
        }

        /**
         * Amplía el tamaño de un vector según un factor y lo compacta.
         * Los nuevos elementos ampliados quedan vacíos a la derecha.
         * Los datos originales se sitúan en la parte izquierda -desde el principio-
         * manteniendo el orden original, dejando todo el espacio libre en la parte derecha.
         *
         * @param factorAmpliacion
         * @return - el número de huecos eliminados.
         */

        public static int ampliarCompactandoIzquierdaDatosUsuarios(int factorAmpliacion) {
            int huecosEliminados = 0;

            // Crea un vector del nuevo tamaño.
            Usuario[] datosUsuariosAmpliado = new Usuario[datosUsuarios.length * factorAmpliacion];

            //TERMINAR CON FOTO
            return huecosEliminados;
        }
    }


