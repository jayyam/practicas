package practica5;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex15
{
    public static void main(String[] args)
    {
        String texto = "01 a2a 223";//texto con numeros y letras
        String regex = "\\d+";//Comprueba si contiene al menos un grupo de caracteres numericos

        Pattern patron = Pattern.compile(regex);
        Matcher concordancias = patron.matcher(texto);

        System.out.println("\nConcordancias: ");
        while (concordancias.find()) {
            System.out.println("Indice: "
                    + concordancias.start()
                    + " Texto: " + concordancias.group()
                    + ")");
        }
    }
}
