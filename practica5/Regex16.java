package practica5;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex16
{
    public static void main(String[] args)
    {
        String texto = "abcd ggg";//texto con numeros y letras
        String regex = "abc.*";//Comprueba si contiene el patron abc
                                // opcionalmente seguido de ninguno o varios caracteres

        Pattern patron = Pattern.compile(regex);
        Matcher concordancias = patron.matcher(texto);

        System.out.println("\nConcordancias: ");
        while (concordancias.find()) {
            System.out.println("Indice: "
                    + concordancias.start()
                    + " Texto: " + concordancias.group()
                    + ")");
        }
    }
}

