package practica5;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex21
{
    public static void main(String[] args)
    {
        String texto = "abc dfdgfdg asdrabcty";//texto con numeros y letras
        String regex = "^[^\\d].*";//Comprueba si no empieza por un digito

        Pattern patron = Pattern.compile(regex);
        Matcher concordancias = patron.matcher(texto);

        System.out.println("\nConcordancias: ");
        while (concordancias.find()) {
            System.out.println("Indice: "
                    + concordancias.start()
                    + " Texto: " + concordancias.group()
                    + ")");
        }
    }
}