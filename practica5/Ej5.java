package practica5;

public class Ej5
{
    public static void main(String[] args)
    {
        boolean mayus = false;
        boolean etiqueta = false;
        String texto = "Estamos viviendo en un <mayus>submarino amarillo</mayus>. No tenemos <mayus>nada</mayus> qué hacer\"";
    }

    static void PasarMayúsculaSubCadena()
    {
        /**
            FORMA LARGA:
            Ir cambiandoo de true a false dependiendo del componente de la etiqueta que se va encontrando
            <>texto</>
            Transformar a mayusculas las palabras entre etiquetas
            ------------------
            FORMA CORTA:
            Expresiones regulares --> <mayus>|</mayus>
         */
    }

}
