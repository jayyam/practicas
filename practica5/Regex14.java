package practica5;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex14
{
    public static void main(String[] args)
    {
        String texto = "bab";//texto con numeros y letras
        String regex = "[b?]";//Comprueba donde aparece la b

        Pattern patron = Pattern.compile(regex);
        Matcher concordancias = patron.matcher(texto);

        System.out.println("\nConcordancias: ");
        while (concordancias.find()) {
            System.out.println("Indice: "
                    + concordancias.start()
                    + " Texto: " + concordancias.group()
                    + ")");
        }
    }
}