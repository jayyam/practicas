package practica5;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex23
{
    public static void main(String[] args)
    {
        String texto = "abc dfdgfdg asdrabcty";//texto con numeros y letras
        String regex = "(a|b)+";//Comprueba si contiene los caracteres "a" , "b" o "ab"

        Pattern patron = Pattern.compile(regex);
        Matcher concordancias = patron.matcher(texto);

        System.out.println("\nConcordancias: ");
        while (concordancias.find()) {
            System.out.println("Indice: "
                    + concordancias.start()
                    + " Texto: " + concordancias.group()
                    + ")");
        }
    }
}