package practica5;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex09
{
    public static void main(String[] args)
    {
        String texto = " 0123*aadd_  + d1?f";//texto con numeros y letras
        String regex = "\\b";//limite de una palabra. indica en que empieza o acaba
                            // una palabra separada por espacios en blanco o caracteres especiales"

        Pattern patron = Pattern.compile(regex);
        Matcher concordancias = patron.matcher(texto);

        System.out.println("\nConcordancias: ");
        while (concordancias.find()) {
            System.out.println("Indice: "
                    + concordancias.start()
                    + " Texto: " + concordancias.group()
                    + ")");
        }
    }
}