package practica5;

public class sus6
{
	/**
	 * Guía de trabajo
	 * 2. Introducción al desarrollo orientado a objetos y las estructuras de datos
	 * La fase de envío consta de dos partes:
	 * 1. Realizar el supuesto práctico.
	 *
	 * Consiste en la implementación de una clase llamada Utilidades, la cual tendrá 3 métodos con los cuales se podrá validar el formato de un email, el formato de una contraseña y si una fecha en formato texto es válida.
	 *
	 * Un ejemplo de funcionamiento sería el siguiente, en el cual todos los datos son válidos:
	 *
	 * Introduce un email: david@gmail.com
	 * El email david@gmail.com es valido
	 *
	 * Introduce un password: MiContraseña*1234
	 * El formato de password es valido
	 *
	 * Introduce una fecha en formato dd/mm/aaaa: 15/03/1999
	 * La fecha 15/03/1999 es valida
	 *
	 * Un ejemplo donde todos los datos no son válidos sería el siguiente:
	 * Introduce un email: david..munuera@gmail.com
	 * El email david..munuera@gmail.com no es valido
	 *
	 * Introduce un password: contraseña123
	 * El password debe tener al menos 8 caracteres, una mayuscula, una minuscula, un numero y un caracter especial
	 *
	 * Introduce una fecha en formato dd/mm/aaaa: 31/04/2022
	 * La fecha 31/04/2022 no es valida
	 *
	 *
	 * Se pide:
	 *
	 *     Dentro de la clase main(), se deberá pedir por teclado los campos para validar con los métodos de la clase Utilidades.
	 *
	 * Detalles:
	 *
	 * Principal
	 *
	 * En la clase Principal se probarán los métodos de la clase Utilidades.
	 *
	 * Atributos
	 *
	 *
	 * Métodos
	 *
	 *
	 * main()
	 *
	 *
	 *     Se debén probar los métodos de la clase Utilidades desde aquí.
	 *     Hay que pedir por teclado los datos que se van a validar.
	 *
	 * Hay que tener en cuenta que:
	 *
	 *     El método pedido debe tener una cabecera de comentarios conteniendo:
	 *         Descripción completa indicando argumento que recibe y valor de retorno.
	 *         Autor
	 *     En la escritura del código fuente se deben seguir las recomendaciones y estilo estándar del lenguaje.
	 *
	 * Utilidades
	 *
	 * La clase Utilidades e contiene métodos estáticos los cuales se utilizan para validar datos.
	 *
	 * Atributos
	 *
	 *
	 * regexEmail
	 *
	 *
	 * Campo estático de tipo String que se usará para validar un email en el método validarEmail. La expresión regular para validar un email es la siguiente:
	 *
	 * ^[\\w-\\+]+(\\.[\\w-\\+]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$
	 *
	 * regexPassword
	 *
	 *
	 * Campo estático de tipo String que se usará para validar una contraseña en el método validarPassword. La expresión regular para validar una contraseña con al menos 8 caracteres, una mayúscula, una minúscula, un número y un caracter especial, es la siguiente:
	 *
	 * (?=.*[A-ZÑ])(?=.*[a-zñ])(?=.*\\d)(?=.+[$*-+&!?%]).{8,}
	 *
	 * Métodos
	 *
	 *
	 * validarEmail()
	 *
	 *
	 *     Método estático que se utilizará para validar un email, que se recibirá como un parámetro de tipo String.
	 *     La propiedad de la clase regexEmail se usará para validar el email recibido.
	 *     El método devolverá true si el email es válido, si no es válido, devolverá false.
	 *
	 * validarPassword()
	 *
	 *
	 *
	 *     Método estático que se utilizará para validar una contraseña, que se recibirá como un parámetro de tipo String.
	 *     La propiedad de la clase regexPassword se usará para validar la contraseña recibida.
	 *     El método devolverá true si la contraseña es válida, si no es válida, devolverá false.
	 *
	 * validarFecha()
	 *
	 *
	 *
	 *     Método estático que se utilizará para validar una fecha, la cual se recibirá como un parámetro de tipo String, en formato dd/mm/aaaa.
	 *     La fecha hay que separarla para tener el día, mes y año por separado.
	 *     Los meses de 31 días son: Enero, Marzo, Mayo, Julio, Agosto, Octubre y Diciembre
	 *     Los meses de 30 días son: Abril, Junio, Septiembre y Noviembre
	 *     El único més de 28 dias es: Febrero. No es necesario comprobar si el año es bisiesto
	 *     El método devolverá true si la fecha es válida, de lo contrario devolverá false.
	 */
}
