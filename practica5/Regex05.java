package practica5;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex05
{
    public static void main(String[] args)
    {
        String texto = "aa.ssdd.asddd.asd";//texto con numeros y letras
        String regex = "\\.";//comprueba si contiene puntos con el patron = (\.)"
        //El patron tiene doble contrabarra = (\\.) ya que la contrabarra es un caracter especial

        Pattern patron = Pattern.compile(regex);
        Matcher concordancias = patron.matcher(texto);

        System.out.println("\nConcordancias: ");
        while(concordancias.find())
        {
            System.out.println("Indice: "
                    + concordancias.start()
                    + " Texto: " + concordancias.group()
                    + ")");
        }
    }
}