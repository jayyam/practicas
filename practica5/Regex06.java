package practica5;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex06
{
    public static void main(String[] args)
    {
        String texto = "abc de mareo";//texto con numeros y letras
        String regex = "mar.";//comprueba si contiene el patron ("mar") +
        // (.) que significa cualquier otro caracter

        Pattern patron = Pattern.compile(regex);
        Matcher concordancias = patron.matcher(texto);

        System.out.println("\nConcordancias: ");
        while(concordancias.find())
        {
            System.out.println("Indice: "
                    + concordancias.start()
                    + " Texto: " + concordancias.group()
                    + ")");
        }
    }
}