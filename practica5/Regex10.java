package practica5;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex10
{
    public static void main(String[] args)
    {
        String texto = "JavaWorld";//texto con numeros y letras
        String regex = "[0-9a-z]";//Comprueba si contiene de 0 -> 9 y de a -> z en minusculas"

        Pattern patron = Pattern.compile(regex);
        Matcher concordancias = patron.matcher(texto);

        System.out.println("\nConcordancias: ");
        while (concordancias.find()) {
            System.out.println("Indice: "
                    + concordancias.start()
                    + " Texto: " + concordancias.group()
                    + ")");
        }
    }
}