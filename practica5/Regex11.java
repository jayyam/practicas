package practica5;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex11
{
    public static void main(String[] args)
    {
        String texto = "JavaWorld";//texto con numeros y letras
        String regex = "a[Ww]";//Comprueba si la cadena el grupo -> a[ seguido de una w o W"]

        Pattern patron = Pattern.compile(regex);
        Matcher concordancias = patron.matcher(texto);

        System.out.println("\nConcordancias: ");
        while (concordancias.find()) {
            System.out.println("Indice: "
                    + concordancias.start()
                    + " Texto: " + concordancias.group()
                    + ")");
        }
    }
}