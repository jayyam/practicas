package practica5;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex25
{
    public static void main(String[] args)
    {
        String email = "pepe.al@gmail.com";//texto con numeros y letras
        String regex = "^[\\w-\\+]+(\\.[\\w-\\+]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";//Comprueba si contiene
                        //comprobando email

        if(email.matches(regex))
        {
            System.out.println("FORMATO DE EMAIL VALIDO");
        }

        else
        {
            System.out.println("FORMATO DE EMAIL NO VALIDO");
        }
    }
}
