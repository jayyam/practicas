package practica5;

import java.util.regex.Pattern;
import java.util.regex.Matcher;


public class Regex01
{
    public static void main(String[] args)
    {
        String texto = "012a9c";//texto con numeros y letras
        String regex = "\\d";//cualquier numero del 0 al 9. Equivale a (0-9)

        Pattern patron = Pattern.compile(regex);
        Matcher concordancias = patron.matcher(texto);

        System.out.println("\nConcordancias: ");
            while(concordancias.find())
            {
                System.out.println("Indice: "
                        + concordancias.start()
                        + " Valor: " + concordancias.group()
                        + ")");
            }
    }
}
